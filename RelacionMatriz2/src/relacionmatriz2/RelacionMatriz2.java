
package relacionmatriz2;


public class RelacionMatriz2 {

    
    public static void main(String[] args) {
        String[][] matriz=new String[2][3];
        String[][] matrizDescifrada=new String[2][3];
        String[][] matrizDescifradaInversa=new String[2][3];
        
        matriz=valoresIniciales(matriz);
        
        System.out.println("Matriz Descifrada:");
        System.out.println(imprimirMatriz(descifrarMatriz(matriz, matrizDescifrada))); // Ejercicio 3
        
        System.out.println("Matriz Descifrada a la Inversa:");
        System.out.println(imprimirMatriz(descifrarMatrizInversa(matrizDescifrada, matrizDescifradaInversa))); // Ejercicio 4
        
    }
    
    /**
     * Funcion que inserta valores iniciales en un array (bidimensional) de String
     * @param m Array de String
     * @return El array con los valores de la matriz
     */
    public static String[][] valoresIniciales(String[][] m) {
        m[0][0]="yo quird!";
        m[0][1]="descifero";
        m[0][2]="passworar";
        m[1][0]="techo";
        m[1][1]="mueño";
        m[1][2]="sungo";
        
        return m;
    }
    
    /**
     * Funcion que imprime el Array de String
     * @param m Array de String
     * @return El Array de String en forma de String
     */
    public static String imprimirMatriz(String[][] m) {
        String s="";
        
        for (int i=0; i<m.length; i++) {
            for (int j=0; j<m[i].length; j++) {
                s+=m[i][j]+"  ";
            }
            s+="\n";
        }
        
        return s;
    }
    
    /**
     * Funcion que descifra constraseñas
     * @param m Array Inicial que se descifra
     * @param mD Resultado del array descifrado
     * @return Array descifrado
     */
    public static String[][] descifrarMatriz(String[][] m, String[][] mD) {
        String aux="";
        String aux2;
        
        for (int i=0; i<mD.length; i++) {
            switch (i) {
                case 0:
                    aux2="";
                    for (int j=mD[i].length-1; j>=0; j--) {
                        if (aux2.equals("")) {
                            aux=m[i][j].substring(m[i][j].length()-3, m[i][j].length());
                        } else {
                            aux=aux2;
                        }
                        if (j!=0) {
                            aux2=m[i][j-1].substring(m[i][j-1].length()-3, m[i][j-1].length());
                            mD[i][j-1]=m[i][j-1].replace(m[i][j-1].substring(m[i][j-1].length()-3, m[i][j-1].length()), aux);
                        } else if (j==0) {
                            mD[i][m[i].length-1]=m[i][m[i].length-1].replace(m[i][m[i].length-1].substring(m[i][m[i].length-1].length()-3, m[i][m[i].length-1].length()), aux);
                        }
                    }
                break;
                case 1:
                    aux2="";
                    for (int j=0; j<m[i].length; j++) {
                        if (aux2.equals("")) {
                            aux=m[i][j].substring(m[i][j].length()-3, m[i][j].length());
                        } else {
                            aux=aux2;
                        }
                        if (j!=2) {
                            aux2=m[i][j+1].substring(m[i][j+1].length()-3, m[i][j+1].length());
                            mD[i][j+1]=m[i][j+1].replace(m[i][j+1].substring(m[i][j+1].length()-3, m[i][j+1].length()), aux);
                        } else if (j==2) {
                            mD[i][0]=m[i][0].replace(m[i][0].substring(m[i][0].length()-3, m[i][0].length()), aux);
                        }
                    }
                break;
                default:
                    break;
            }
        }
        
        return mD;
    }
    
    /**
     * Funcion que descifra la matriz de forma invertida a la funcion descifrarMatriz()
     * @param m Array Inicial de la matriz descifrada
     * @param mD Resultado de la descifracion inversa
     * @return Matriz descifrado de forma invertida
     */
    public static String[][] descifrarMatrizInversa(String[][] m, String[][] mD) {
        String aux="";
        String aux2;
        
        for (int i=0; i<mD.length; i++) {
            switch (i) {
                case 1:
                    aux2="";
                    for (int j=mD[i].length-1; j>=0; j--) {
                        if (aux2.equals("")) {
                            aux=m[i][j].substring(m[i][j].length()-3, m[i][j].length());
                        } else {
                            aux=aux2;
                        }
                        if (j!=0) {
                            aux2=m[i][j-1].substring(m[i][j-1].length()-3, m[i][j-1].length());
                            mD[i][j-1]=m[i][j-1].replace(m[i][j-1].substring(m[i][j-1].length()-3, m[i][j-1].length()), aux);
                        } else if (j==0) {
                            mD[i][m[i].length-1]=m[i][m[i].length-1].replace(m[i][m[i].length-1].substring(m[i][m[i].length-1].length()-3, m[i][m[i].length-1].length()), aux);
                        }
                    }
                break;
                case 0:
                    aux2="";
                    for (int j=0; j<m[i].length; j++) {
                        if (aux2.equals("")) {
                            aux=m[i][j].substring(m[i][j].length()-3, m[i][j].length());
                        } else {
                            aux=aux2;
                        }
                        if (j!=2) {
                            aux2=m[i][j+1].substring(m[i][j+1].length()-3, m[i][j+1].length());
                            mD[i][j+1]=m[i][j+1].replace(m[i][j+1].substring(m[i][j+1].length()-3, m[i][j+1].length()), aux);
                        } else if (j==2) {
                            mD[i][0]=m[i][0].replace(m[i][0].substring(m[i][0].length()-3, m[i][0].length()), aux);
                        }
                    }
                break;
                default:
                break;
            }
        }
        
        return mD;
    }
}
