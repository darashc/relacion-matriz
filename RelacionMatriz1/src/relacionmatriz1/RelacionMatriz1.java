
package relacionmatriz1;

import java.util.Scanner;


public class RelacionMatriz1 {

    /**
     * Funcion main que ejecuta el ejercicio 1 y 2 de la Relacion de las Matrices
     * @param args 
     */
    public static void main(String[] args) {
        String[][] matriz=new String[3][3];
        int f1, c1, f2, c2;
        
        // Ejercicio 1
        String mInicial=imprimirMatriz(valoresIniciales(matriz));
        System.out.println("Matriz inicial:");
        System.out.println(mInicial);
        String mOrdenada=imprimirMatriz(ordenarMatriz(matriz));
        System.out.println("Matriz ordenada:");
        System.out.println(mOrdenada);
        // Fin Ejercicio 1
        
        // Ejercicio 2
        do {
            f1=introducirEntero("Introduzca la fila para la posicion 1");
            c1=introducirEntero("Introduzca la columna para la posicion 1");
        } while (f1>=matriz.length||c1>=matriz[f1].length); // Comprobacion para que el valor de fila y columna introducida sea correcta
        do {
            f2=introducirEntero("Introduzca la fila para la posicion 2");
            c2=introducirEntero("Introduzca la columna para la posicion 2");
        } while (f2>=matriz.length||c2>=matriz[f2].length); // Comprobacion para que el valor de fila y columna introducida sea correcta
        
        String mIntercambiada=imprimirMatriz(intercambiarPosiciones(valoresIniciales(matriz), f1, c1, f2, c2));
        System.out.println("\nMatriz Intercamiada:");
        System.out.println(mIntercambiada);
        // Fin Ejercicio 2
        
    }
    
    /**
     * Funcion que inserta valores iniciales en un array (bidimensional) de String
     * @param m Array de String
     * @return El array con los valores de la matriz
     */
    public static String[][] valoresIniciales(String[][] m) {
        m[0][0]="muchos";
        m[0][1]="ahora";
        m[0][2]="vez";
        m[1][0]="la";
        m[1][1]="y capaces";
        m[1][2]="a";
        m[2][0]="de trabajar mucho";
        m[2][1]="sois";
        m[2][2]="alumnos";
        
        return m;
    }
    
    /**
     * Funcion que consiste en ordenar la matriz de menor a mayor en funcion de la longitud de los caracteres
     * @param m Array de String
     * @return El array de String ordenado de menor a mayor en funcion de la longitud de los caracteres
     */
    public static String[][] ordenarMatriz(String[][] m) {
        int longitud;
        String aux="";
        
        for (int i=0; i<m.length; i++) {
            for (int j=0; j<m[i].length; j++) {
                longitud=longitudString(m, i, j);
                for (int x=0; x<m.length; x++) {
                    for (int y=0; y<m[x].length; y++) {
                        if (x==i&&y==j) {
                            aux=m[x][y];
                        }
                        if (m[x][y].length()==longitud) {
                            m[i][j]=m[x][y];
                            m[x][y]=aux;
                        }
                    }
                }
            }
        }
        return m;
    }
    
    /**
     * Funcion que imprime el Array de String
     * @param m Array de String
     * @return La matriz en forma de una variable String
     */
    public static String imprimirMatriz(String[][] m) {
        String s="";
        
        for (int i=0; i<m.length; i++) {
            for (int j=0; j<m[i].length; j++) {
                s+=m[i][j]+"  ";
            }
            s+="\n";
        }
        
        return s;
    }
    
    /**
     * Funcion que calcula la longitud de un String en cualquier posicion de la matriz
     * Se utiliza en la función ordenarMatriz()
     * @param m Array introducido
     * @param a Fila del array
     * @param b Columna del array
     * @return El valor de la longitud de String de dicha posicion en el array
     */
    public static int longitudString(String[][] m, int a, int b) {
        int l=100;
        
        for (int i=a; i<m.length; i++) {
            if (i!=a) {
                b=0;
            }
            for (int j=b; j<m[i].length; j++) {
                if (m[i][j].length()<l) {
                    l=m[i][j].length();
                }
            }
        }
        
        return l;
    }
    
    /**
     * Funcion que intercamia los valores del String entre 2 posiciones pedidios por teclado
     * @param m Array de String
     * @param f1 Fila de la posicion 1
     * @param c1 Columna de la posicion 1
     * @param f2 Fila de la posicion 2
     * @param c2 Columna de la posicion 2
     * @return El Array con los valores intercambiados
     */
    public static String[][] intercambiarPosiciones(String[][] m, int f1, int c1, int f2, int c2) {
        String aux=m[f1][c1];
        m[f1][c1]=m[f2][c2];
        m[f2][c2]=aux;
        
        return m;
    }
    
    /**
     * Funcion que te permite introducir un entero por teclado
     * @param m Mensaje
     * @return Un entero
     */
    public static int introducirEntero(String m) {
        Scanner s=new Scanner(System.in);
        
        System.out.println(m);
        return s.nextInt();
    }
}
